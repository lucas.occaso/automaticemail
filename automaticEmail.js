function getData(){
  const sheet = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1BhuBYV5dLC7lEVdiO8rgQB4QeWFamlg5UxyeTa4TSFQ/edit#gid=0')
  const numberOfRows = sheet.getLastRow()
  const todayName = day_name(new Date())
  const todayNumber = new Date().getDate().toString()
  const thisHour = new Date().getHours().toString()

  for(i = 2; i <= numberOfRows; i++){
    try{
      let day = sheet.getRange('B'+ i).getValues().toString().split(",")
      
      if(day.includes(todayName) || day.includes(todayNumber)){
        let hour = sheet.getRange('C'+ i).getValues().toString().split(",")
        
        
        if(hour.includes(thisHour)){
          let sender = sheet.getRange('A'+ i).getValues().toString()  
          let title = sheet.getRange('D'+ i).getValues().toString()    
          let numberOfColumns = sheet.getLastColumn()
          let body = []
          
          if(sheet.getRange('E'+ i).getValues().toString() == 'sim'){
            title = title +' - '+ month_name(new Date())
          }
          
          receiver = sheet.getRange('F'+ i).getValues().toString().split(",")
          
          let values = []
          for(j = 7; j <= numberOfColumns; j++ ){
            body.push(
              sheet.getSheetValues(i, j, 1, 1)
            )
          }
          emailSender(title, body, receiver, sender)
        }
      }
    }catch(e){
      Logger.log(e)
    }
  }
}

function emailSender(title, body, receiver, sender){  

  const html = []
  const formattedBody = body.map(text => (
    `<p>${text}</p>`
  )).join(' ')
  const formatedReceiver = receiver.map(text => (text)).join(',')
  
  
  html.push(
    "<html>",
      "<head>",
        "<meta name='viewport' content='width=device-width' />",
        "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />",
        "<title>",
          title,
        "</title>",
      "</head>",
      "<body>",
         formattedBody, 
      "</body>",
    "</html>"
  )
  
  if(sender == ''){
    sender = 'Email Automático'
  }
  
  var message = {
    to: formatedReceiver,
    subject: title,
    htmlBody: html.join('\n'),
    name: sender
  }
  Logger.log(message)
  MailApp.sendEmail(message)
}

var month_name = function(dt){
  mlist = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ];
  return mlist[dt.getMonth()];
};
var day_name = function(dt){
  dlist = [ "domingo", "segunda", "terça", "quarta", "quinta", "sexta", "sabado"];
  return dlist[dt.getDay()];
};
